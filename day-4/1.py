'''
Validate Ipv4 or IPv6 address
'''

import re
import sys
import socket

def validateIP(ipAddress) :
	'''
	Check if given parameter is a valid IP address

	Input : ipAddress string
	Returns : True if ipAddress is valid IP address and False otherwise
	'''
	matching = re.match(r'^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$', ipAddress)
	if matching :
		print "IPv4"
		blocks = matching.groups()

		for block in blocks :
			if int(block) > 255 :
				return False
	else :
		print "Not IPv4"
		if re.match(r'([0-9A-Fa-f]{1,4}:){7}:[0-9A-Fa-f]' , ipAddress) :
			return True
		try:
		    socket.inet_pton(socket.AF_INET6,ipAddress)
		    print "Valid IPv6"
		    return True
		except:
		    print "Invalid IP address exception"
		    return False
	return True

ipAddress = str(raw_input("Enter the IP address : "))
if validateIP(ipAddress) :
	print "Valid IP address"
else :
	print "Invalid IP address"
