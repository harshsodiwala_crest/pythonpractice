def findFrequency(string, ch) :
	'''
	Finds the frequency of character 'ch' in string 'string'
	
	Input : string 'string' and character 'ch'
	Returns : Integer that is the calculated frequency
	'''
	# if only alphabets and numbers are allowed
	# string = string.replace("[^a-zA-Z0-9]","")
	# return len(string)

	#if any character are to be allowed
	frequency = 0
	for letter in string :
		if letter == ch :
			frequency += 1

	return frequency

#driver
#get string and the character
string = str(raw_input("Enter a string : "))
ch = str(raw_input("Find frequency of : "))
while len(ch != 1) :
	print
	ch = str(raw_input("Please enter only one character : "))

#find and print the frequency
frequency = findFrequency(string, ch)
print ch + "occurs " + str(frequency) + "times in the string"
