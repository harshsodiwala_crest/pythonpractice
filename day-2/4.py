def shortenString(string) :
        '''
        Shorten the string as :
                Removes the characters at odd index

        Input : string - 'string'
        Returns : string - the shortened string
        '''

        i = 0
        shortenedString = ""
        while i < len(string) :
                shortenedString = shortenedString + string[i]
                i = i+2

        return shortenedString

#driver
string = str(raw_input("Enter a string : "))
shortenedString = shortenString(string)
print "Shortened string : ", shortenedString