def insertIntoString(baseString, insertString) :
	'''
	Insert a string in the middle of another given string

	Input : string - 'baseString' - The string into which other string is to be inserted
			string - 'insertString' - The string that is to be inserted in the middle of 'baseString'
	Output : The string generated after the above opetration
	'''

	generatedString = baseString[ : (len(baseString)/2)] + insertString + baseString[(len(baseString)+1)/2 : ]
	return generatedString

#driver
baseString = str(raw_input("Enter a string : "))
insertString = str(raw_input("Enter the string to be inserted: "))

finalString = insertIntoString(baseString, insertString)
print finalString