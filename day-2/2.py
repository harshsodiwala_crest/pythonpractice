def generateString(string) :
        '''
        Genarates the string made from first two and the last two
        letters of the given string

        Input : string - 'string';
        Output : string - the generated string
        '''

        #notify if string is lesser than 2 characters
        if len(string) < 2 :
                return "Empty string"

        #generate and return the string
        generatedString = string[0:2] + string[len(string)-2 : len(string)-1]
        return generatedString

#driver(
string = str(raw_input("Enter a string : "))
genString = generateString(string)
print genString