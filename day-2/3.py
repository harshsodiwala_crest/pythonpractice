def upgradeString(string) :
        '''
        Process the string as :
                Add 'ing' at the end of a string
                If the stirng already ends with 'ing' then add 'ly'
                If the string is shorter than 2 characters then leave it unchanged

        Input : string - 'string'
        Returns : string - the processed string
        '''

        #for strings with length less than 3
        if len(string) < 3 :
                return string

        if string.endswith("ing") :
                string = string + "ly"
        else :
                string = string + "ing"

        return string

#driver
string = str(raw_input("Enter a string : "))
upgradedString = upgradeString(string)
print "Upgraded string : ", upgradedString