###Day 2 :

Topics
```
1. Working on files
2. Loops
3. String operations
```

####Tasks

Task 1
>Write a python function which counts the frequency of given character in a given string.
>Inputs - A String
>A Character whose frequency needs to be determined

---

Task 2
>Write a Python program to get a string made of the first 2 and the last 2 chars from a given a string.
>If the string length is less than 2, return "Empty String"

---

Task 3
>Write a Python program to add 'ing' at the end of a given string (length should be at least 3).
>If the given string already ends with 'ing' then add 'ly' instead.
>If the string length of the given string is less than 3, leave it unchanged.

---

Task 4
>Write a Python function to remove the characters which have odd index values of a given string.

---

Task 5
>Write a Python function to insert a string in the middle of a string.
>For odd length of string, remove the middle character and replace with given string.

