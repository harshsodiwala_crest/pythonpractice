#function to swap 2 integers
def swap(a, b):
	a,b = b,a
	return [a,b]

a = int(raw_input("Enter number 1 :"))
b = int(raw_input("Enter number 2 :"))

#print the numbers
print "You entered", a, "and", b

#swap the numbers
a, b = swap(a,b)

#print
print
print "... Swapping ..."
print "1st number : " + str(a)
print "2nd number : " + str(b)
