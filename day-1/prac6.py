
#get inputs
N,C,M = raw_input().split()
N = int(N)
C = int(C)
M = int(M)

flag = True
#get number of containers for each trip
containers = raw_input().split()
for i in containers:
	if int(i) > M*C:
		flag = False

if flag :
        print "Yes"
else :
        print " No"
