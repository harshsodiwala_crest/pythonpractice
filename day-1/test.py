fileName = str(raw_input("Enter file : "))
fd = open(fileName)

paragraph = fd.read()
words = paragraph.replace('\n', ' ').split(" ")

d= dict()

for i in words:
   	if i in d:
    	    d[i] += 1
   	else :
   	    d[i] = 1

sortedVals = d.values()
sortedVals.sort(reverse=True)

print "Maximum appearing word : "
print d.keys()[d.values().index(sortedVals[0])], " : " ,d[d.keys()[d.values().index(sortedVals[0])]]
d.pop(d.keys()[d.values().index(sortedVals[0])])
sortedVals.pop(0)

for i in sortedVals:
	print d.keys()[d.values().index(i)], " : " ,d[d.keys()[d.values().index(i)]]
	d.pop(d.keys()[d.values().index(i)])

