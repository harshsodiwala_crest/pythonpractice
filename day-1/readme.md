#Day 1#

```test.py``` is a practice script I made. It shows how many time each word is repeated in a user given file. Also prints the most repeated words. Got to learn cool stuff like playing with key-values in dictionary to obtain sorted dictionary.

#### The given questions were following :

1) Write python script to take no of arguments as input from the user. Then read no of arguments from the standard input.
Print read arguments  on output .



2)  Write python script to take one integer argument and then print as follows:
	- If Value >0 and Value < 10 — Small
	- If Value > 10 and Value <100 — Medium
	- If Value <1000 — Large
	- If Value > 1000 — Invalid

 

3) Write a function to find larger of two numbers



4) Write a program to take two numbers as input parameter and then ask for the arithmetic parameter to be performed.
i.e “Enter Two numbers”
10 45
“Operations to perform “   +
Sum is 55

5) Write a program to take two integers  as input. Print those two integers as output and then call a function to swap those two
integers.



6) Paresh owns a company that moves containers  between two islands. He has N trips booked, 
and each trip  has P containers.
Paresh has M boats for transporting containers, and each boat's maximum capacity is C  containers.
Given the number of containers going on each trip, determine whether or not Paresh can perform all  trips using no more than
M boats per individual trip. 
If this is possible, print Yes; otherwise, print No

    Input Format

    The first line contains three space-separated integers describing the respective values of  N(number of trips),  C (boat capacity),
    and  M(total number of boats). 
    The second line contains  space-separated integers describing the value for container for each
    trip.


    Constraints
    * 1<= m,c ,p<=100


    Output Format
    
    Print Yes if Paresh can perform 
    booked trips using no more than boats per trip; otherwise, print No.
    
    Sample Input 0
    5 2 2
     1 2 1 4 3
     Sample Output 0
    Yes 
    
    Sample Input 0
    5 1 2
     1 2 1 4 3
    Sample Output 0 
    No
