import re

def is_pangram(sentence) :
	news = set(re.sub("[^a-zA-Z]", "", sentence))
	if len(news) == 26 : 
		return True
	return False
