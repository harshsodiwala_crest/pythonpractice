import re
def to_rna(dna_strand):
    valid = "ACGT"
    dna_strand_new = ""
    mapp = {"C":"G" , "G":"C", "T":"A", "A":"U",}
    if len(re.sub("[^CGTA]","",dna_strand)) != len(dna_strand) :
    	raise(ValueError)
    else :
    	for i in range(0,len(dna_strand)) :
    		dna_strand_new = dna_strand_new + str(mapp[dna_strand[i]])
    	return dna_strand_new

