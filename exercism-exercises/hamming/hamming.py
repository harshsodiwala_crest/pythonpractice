def distance(strand_a, strand_b):
    if len(strand_b) != len(strand_a) :
    	raise(ValueError)
    else :
    	diff = 0
    	for i in range(len(strand_a)) :
    		if strand_a[i] != strand_b[i] :
    			diff = diff + 1
    	return diff
