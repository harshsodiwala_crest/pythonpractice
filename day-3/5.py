'''
Write a program to read names and grades of each student in class of N student.
Store these details into nested list.
Write a function to find student with second highest marks.
'''

from operator import itemgetter

def getSecondHighest(lst) :
	'''
	Finds the student with second highest marks

	Input : A 2D list that stores [name,grades] for each student
	Returns : A list [name.grades] that represent the student with 2nd higheat grades
	'''

	if len(lst) < 2 :
		return lst[0]
	else :
		#sort the list with marks as key
		lst.sort(key = itemgetter(1))
		return lst[1]

#driver
try :
	#get the student data
	numOfStudents = int(raw_input("Enter the number of students : "))
	studentList = []
	for i in range(numOfStudents) :
		name = str(raw_input("Name : "))
		grades = int(raw_input("Grades : "))
		print
		studentList.append([name,grades])

	#find and display the student with 2nd highest marks
	secondStudent = getSecondHighest(studentList)
	print "The student with 2nd highest grades is :"
	print secondStudent[0] + " with " + str(secondStudent[1]) + " marks ..!"


except ValueError :
	print "You got confused while entering a number"
