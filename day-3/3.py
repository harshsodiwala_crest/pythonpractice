'''
This program has a two way handling of type checking. The driver part validates the input to be integer
as well as the function findAvgList does exception handling.
This ensures that even if the function is imported and used in a different file, the integer rule
does apply
'''

def findAvgList(lst) :
	'''
	Finds the average of the numbers in the given list.
	Allows lists with only numbers

	Input : a list
	Returns : average of the numebrs in the list
	'''
	try :
		sum = 0
		for i in lst :
			if type(i) != int :
				raise ValueError
			else :
				sum += i
		average = float(sum) / len(lst)
		return average

	except ValueError :
		print "The list you provided contains non-arithematic values"

#driver
try :
	#creates the list
	numOfNum= int(raw_input("How many numbers do you need ? "))

	lst = []
	for i in range(numOfNum) :
		lst.append(int(raw_input("Number " + str(i+1) + " : ")))

	#find and print the average
	print "The average is : " , findAvgList(lst)

except ValueError :
	#if anything other than numbers is entered
	print "Only numbers are allowed around here"

