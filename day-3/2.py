def findIndex(lst, parameter) :
	'''
	Find the index of the given parameter in the list
	
	Input : lst - list to be used for searching
			parameter - the parameter wose index is to be found
	Returns : an integer that is the index of the parameter if found and -1 otherwise
	'''

	if parameter not in lst :
		return - 1
	else :
		return lst.index(parameter)

#driver
print "Creating the list"
print
try :
	global numOfParameters
	numOfParameters = int(raw_input("Enter the number of parameters : "))
except ValueError :
	print "Please enter a number"
else :
	lst = []
	for i in range(numOfParameters) :
		lst.append(raw_input("Enter a parameter : "))

	parameter = raw_input("Enter parameter to be searched for : ")
	print "The parameter is at index number : " , findIndex(lst, parameter) + 1 
