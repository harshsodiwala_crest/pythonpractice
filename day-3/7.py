'''
Create a program to take student information as input. Student will have First Name, Last Name, Roll No.
Write a function to sort the list based on given input parameter. i.e By First Name or Last Name or Roll No. 
'''

from operator import itemgetter

def getSortedList(lst, param) :
	'''
	Finds the student with second highest marks

	Input : A 2D list that stores [fname, lname, rollno] for each student, and parameter to sort with
	Returns : Sorted list
	'''

	choices = {"fname" : 0,"lname" : 1,"rollNo" : 2}

	#sort the list with marks as key
	lst.sort(key = itemgetter(choices[param]))
	return lst

#driver
try :
	#get the student data
	numOfStudents = int(raw_input("Enter the number of students : "))
	studentList = []
	for i in range(numOfStudents) :
		fname = str(raw_input("First Name : "))
		lname = str(raw_input("Last name : "))
		rollNo = int(raw_input("Roll No : "))
		print
		studentList.append([fname,lname,rollNo])

	#get the sorting parameter and sort
	print "Sort acc. to : "
	print "1. First Name"
	print "2. Last Name"
	print "3. Roll Number"

	choices = {1: "fname", 2: "lname", 3: "rollNo"}
	choice = int(raw_input("Enter yout choice : "))

	newList =  getSortedList(studentList, choices[choice])

	print "Sorted list is : "
	for i in newList :
		for j in i :
			print j, 
		print

except ValueError :
	print "You got confused while entering a number"
