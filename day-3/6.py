'''
The ATM  has notes in following denomination : 2000, 500, 100.
'''

import sys

def numOfNotes(amount):
        '''
        Finds the number of each notes required to make given amount

        Input : an integer - the amount
        Returns : A dictionary with denomination as keys and number of them required as value
        '''

        try :
                d = dict()
                if amount % 100 != 0 :
                        print "Enter a multiple of 100"
                        sys.exit()

                #list of notes. Can be changed.
                lstOfNotes = [2000, 500, 100]
                currentDenomination = 0

                #calculate number of each notes needed
                for note in lstOfNotes :
                        if amount > note :
                                d[note] = amount / note
                                amount = amount % note
                        
                return d

        except ValueError :
                print "only numbers please"

amount = int(raw_input("Enter amount : "))
d = numOfNotes(amount)
for i in d :
        print str(i) + " : " + str(d[i])
