'''
Read a sentence from the standard input. Find out how many times each word appear in given string.
'''

import re

def getWordCount(phrase) :
        '''
        Counts the number of occurrence of each word and returns them as a dictionary

        Input : a string
        Returns : a dictionary that stores the count of each word by word as key
        '''
        d = dict()
        phrase = re.sub("[^a-zA-Z'0-9 ]", " ", phrase).lower()
        phrase = re.sub(" '", " ", phrase).lower()
        phrase = re.sub("' ", " ", phrase).lower()

        words = phrase.replace("[^a-zA-Z ]"," ").split()
        print words
        for word in words :
                if word in d :
                        d[word] = d[word] + 1
                else :
                        d[word] = 1
        return d
#print
try :
        wordCountDict = getWordCount(str(raw_input("Enter string : ")))
        for wordCount in wordCountDict :
                print wordCount, wordCountDict[wordCount]
except :
        print "Oops .. something went wrong"
