'''
Write a program to take size of the list as input. Then read as many values as input. Store these details into list and print out list.
'''

try :
	global numOfParameters
	numOfParameters = int(raw_input("Enter the number of parameters : "))
except ValueError :
	print "Please enter only numbers"
else :
	lst = []
	for i in range(numOfParameters) :
		lst.append(raw_input("Enter a parameter : "))

	print
	print "The final list : " , lst
