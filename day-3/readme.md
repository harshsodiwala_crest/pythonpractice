###Day 3 :

Topics
```
1. List
2. Dictionaries
3. Tuples
```

####Tasks

Task 1
>Write a program to take size of the list as input.
>Then read as many values as input.
>Store these details into list and print out list.

---

Task 2
>Write a program to find index of given input parameter in the list. 

---

Task 3
>Write a program to find average of the list.  List should not contain non arithmetic values. 

---

Task 4
>Read a sentence from the standard input. Find out how many times each word appear in given string.


---

Task 5
>Write a program to read names and grades of each student in class of N student.
>Store these details into nested list. Write a function to find student with second highest marks.

---

Task 6
>Write a program a function for ATM machine which takes amount as input and output should be number
>of notes of each denomination.
>The ATM  has notes in following denomination : 2000, 500, 100.

---

Task 7
>Create a program to take student information as input.
>Student will have First Name, Last Name, Roll No.
>Write a function to sort the list based on given input parameter.
>i.e By First Name or Last Name or Roll No.